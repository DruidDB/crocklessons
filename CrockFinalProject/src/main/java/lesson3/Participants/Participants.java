package lesson3.Participants;

public interface Participants {
    public void run(int distance);
    public void jump(int height);
}
