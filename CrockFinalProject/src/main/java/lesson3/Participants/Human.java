package lesson3.Participants;

public class Human implements Participants {
    private String type;
    private String name;
    private int maxRunDistance;
    private int maxJumpHeight;

    public Human(String name, int maxRunDistance, int maxJumpHeight) {
        this.type = "Человек";
        this.name = name;
        this.maxRunDistance = maxRunDistance;
        this.maxJumpHeight = maxJumpHeight;
    }

    @Override
    public void run(int distance) {
        if(maxRunDistance >= distance){
            System.out.println(type + " " + name + "- пробежал дистанцию " + distance + "м.");
        }else {
            System.out.println(type + " " + name + "- не смог пробежать дистанцию " + distance + "м.");
        }
    }

    @Override
    public void jump(int height) {
        if(maxJumpHeight >= height){
            System.out.println(type + " " + name + "- перепрыгнул стену " + height + "м.");
        }else {
            System.out.println(type + " " + name + "- не смог перепрыгнуть стену " + height + "м.");
        }
    }
}
