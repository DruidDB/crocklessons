package lesson3.Obstacles;

import lesson3.Participants.Participants;

public class Treadmill implements Obstacles {
    private int lenght;

    public Treadmill(int lenght) {
        this.lenght = lenght;
    }

    @Override
    public void overcoming(Participants participants) {
        participants.run(lenght);
    }
}
