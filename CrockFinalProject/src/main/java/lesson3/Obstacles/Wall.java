package lesson3.Obstacles;

import lesson3.Participants.Participants;

public class Wall implements Obstacles {
    private int height;

    public Wall(int height) {
        this.height = height;
    }

    @Override
    public void overcoming(Participants participants){
        participants.jump(height);
    }
}
