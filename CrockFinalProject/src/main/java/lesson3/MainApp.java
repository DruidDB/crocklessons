package lesson3;

//1.	Продолжаем работать с участниками и выполнением действий. Создайте три класса Человек, Кот, Робот, которые не наследуются от одного класса. Эти классы должны уметь бегать и прыгать, все также с выводом информации о действии в консоль.
//2.	Создайте два класса: беговая дорожка и стена, при прохождении через которые, участники должны выполнять соответствующие действия (бежать или прыгать), результат выполнения печатаем в консоль (успешно пробежал, не смог пробежать и т.д.). У препятствий есть длина (для дорожки) или высота (для стены), а участников ограничения на бег и прыжки.
//3.	Создайте два массив: с участниками и препятствиями, и заставьте всех участников пройти этот набор препятствий. Если участник не смог пройти одно из препятствий, то дальше по списку он препятствий не идет.

import lesson3.Obstacles.Obstacles;
import lesson3.Obstacles.Treadmill;
import lesson3.Obstacles.Wall;
import lesson3.Participants.Cat;
import lesson3.Participants.Human;
import lesson3.Participants.Participants;
import lesson3.Participants.Robot;

public class MainApp {
    public static void main(String[] args) {
        Participants[] participants = {
                new Cat("Барсик", 150, 5),
                new Human("Игооорь", 2000, 2),
                new Robot("IT-2021", 50, 10)
        };

        Obstacles[] obstacles = {
                new Treadmill(30),
                new Treadmill(120),
                new Treadmill(1500),
                new Wall(1),
                new Wall(3),
                new Wall(12)
        };

        for (Obstacles o : obstacles) {
            for (Participants p : participants) {
                o.overcoming(p);
            }
        }
    }
}
