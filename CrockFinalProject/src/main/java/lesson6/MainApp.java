package lesson6;

//Необходимо написать два метода, которые делают следующее:
//1.	Создают одномерный длинный массив, например:
//static final int SIZE = 10 000 000;
//static final int HALF = size / 2;
//float[] arr = new float[size].
//
//2.	Заполняют этот массив единицами.
//3.	Засекают время выполнения: long a = System.currentTimeMillis().
//4.	Проходят по всему массиву и для каждой ячейки считают новое значение по формуле:
//arr[i] = (float)(arr[i] * Math.sin(0.2f + i / 5) * Math.cos(0.2f + i / 5) * Math.cos(0.4f + i / 2)).
//
//5.	Проверяется время окончания метода System.currentTimeMillis().
//6.	В консоль выводится время работы: System.out.println(System.currentTimeMillis() - a).
//
//Отличие первого метода от второго:
//●	первый просто бежит по массиву и вычисляет значения;
//●	второй разбивает массив на два, в двух потоках вычисляет новые значения и потом склеивает эти массивы обратно в один.
//
//Пример деления одного массива на два:
//●	System.arraycopy(arr, 0, a1, 0, h);
//●	System.arraycopy(arr, h, a2, 0, h).
//
//Пример обратной склейки:
//●	System.arraycopy(a1, 0, arr, 0, h);
//●	System.arraycopy(a2, 0, arr, h, h).
//
//Примечание:
//System.arraycopy() — копирует данные из одного массива в другой:
//System.arraycopy(массив-источник, откуда начинаем брать данные из массива-источника, массив-назначение, откуда начинаем записывать данные в массив-назначение, сколько ячеек копируем).
//
//Замеры времени:
//●	для первого метода надо считать время только на цикл расчёта:
//for (int i = 0; i < size; i++) {
//   arr[i] = (float)(arr[i] * Math.sin(0.2f + i / 5) * Math.cos(0.2f + i / 5) * Math.cos(0.4f + i / 2));
//}
//
//●	для второго метода нужно замерить время разбивки массива на 2, просчёта каждого из двух массивов и склейки

import java.nio.FloatBuffer;

public class MainApp {
    static final int SIZE = 10000000;
    static final int HALF = SIZE / 2;

    public static void main(String[] args) {
        float[] arr = new float[SIZE];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = 1.0f;
        }

        twothreadMethod(arr);
    }

    public static void singleThreadMethod(float[] arr) {

        long startTime = System.currentTimeMillis();

        for (int i = 0; i < arr.length; i++) {
            arr[i] = (float) (arr[i]
                    * Math.sin(0.2f + i / 5)
                    * Math.cos(0.2f + i / 5)
                    * Math.cos(0.4f + i / 2));
        }

        long endTime = System.currentTimeMillis();

        System.out.println(endTime - startTime);
    }

    public static void twothreadMethod(float[] arr) {

        float[] arrL = new float[HALF];
        float[] arrR = new float[HALF];

        long startTime = System.currentTimeMillis();

        System.arraycopy(arr, 0, arrL, 0, HALF);
        System.arraycopy(arr, HALF, arrR, 0, HALF);

        Thread leftThread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < arrL.length; i++) {
                    arrL[i] = (float) (arrL[i]
                            * Math.sin(0.2f + i / 5)
                            * Math.cos(0.2f + i / 5)
                            * Math.cos(0.4f + i / 2));
                }
            }
        });

        Thread rightThread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0, h = HALF; i < arrR.length; i++, h++) {
                    arrR[i] = (float) (arrR[i]
                            * Math.sin(0.2f + h / 5)
                            * Math.cos(0.2f + h / 5)
                            * Math.cos(0.4f + h / 2));
                }
            }
        });

        rightThread.start();
        leftThread.start();

        try {
            rightThread.join();
            leftThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.arraycopy(arrL, 0, arr, 0, HALF);
        System.arraycopy(arrR, 0, arr, HALF, HALF);

        long endTime = System.currentTimeMillis();

        System.out.println(endTime - startTime);
    }

}
