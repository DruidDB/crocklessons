package lesson1;

//1.	Задать целочисленный массив, состоящий из элементов 0 и 1. Например: [ 1, 1, 0, 0, 1, 0, 1, 1, 0, 0 ]. С помощью цикла и условия заменить 0 на 1, 1 на 0.
//2.	Задать пустой целочисленный массив размером 8. С помощью цикла заполнить его значениями 1, 4, 7, 10, 13, 16, 19, 22.
//3.	Создать метод, которому в качестве аргумента передается целочисленный массив. Метод должен элементы массива, которые меньше 6, умножить на 2.
//4.	Написать метод, которому в качестве параметра передается целое число. Метод должен напечатать в консоль, положительное число ему передали или отрицательное. Замечание: ноль считаем положительным числом.
//5.	Написать метод, принимающий на вход два числа и проверяющий, что их сумма лежит в пределах от 10 до 20 (включительно). Если да — вернуть true, в противном случае — false.
//6.	* Задать одномерный массив и найти в нем минимальный и максимальный элементы (без помощи интернета).
//7.	** Написать метод, в который передается непустой одномерный целочисленный массив. Метод должен вернуть true, если в массиве есть место, в котором сумма левой и правой части массива равны. Примеры: checkBalance([2, 2, 2, 1, 2, 2, || 10, 1]) → true, checkBalance([1, 1, 1, || 2, 1]) → true. Граница показана символами ||, которые в массив не входят.
//8.	*** Написать метод, которому на вход подается одномерный массив и число n (может быть положительным или отрицательным), при этом метод должен сместить все элементы массива на n позиций. Для усложнения задачи: нельзя пользоваться вспомогательными массивами.


public class MainApp {
    public static void main(String[] args) {
        //task1();
        //task2();

        //int[] arr = {1,2,3,4,5,6,7,8,9}; // task3 array
        //task3Meth(arr);

        //task4(-5);
        //System.out.println(task5(10,23)); //task6 + check
        //task6();

        //int[] arr = {1, 1, 1, 2, 1}; // task7 array
        //System.out.println(task7Meth(arr)); //task7 + check

        //int[] arr = {0, 1, 2, 3, 4}; // task8 array
        //task8(arr, -3); //task8

    }

    public static void task1() {
        int[] arr = {1, 0, 1, 0, 1, 0, 1};

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                arr[i] = 1;
            } else {
                arr[i] = 0;
            }
        }
        //check
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }//check
    }

    public static void task2() {
        int[] arr = new int[8];
        for (int i = 1; i < arr.length; i++) {
            arr[0] = 1;
            arr[i] = arr[i - 1] + 3;
        }

        //check task2
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    public static void task3Meth(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 6) {
                array[i] *= 2;
            }
        }
        //check
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }

    public static void task4(int number) {
        if (number >= 0) {
            System.out.println("Положительное число");
        } else {
            System.out.println("Отрицательное число");
        }
    }

    public static boolean task5(int number1, int number2) {
        if (number1 + number2 >= 10 && number1 + number2 <= 20) {
            return true;
        }
        return false;
    }

    public static void task6() {
        int[] arr = {5, 7, 1, 8, 3, 6, 4, 3, 8, 4, -41};

        int min = arr[0];
        int max = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        //checkTask6
        System.out.println("Максимальное значение = " + max);
        System.out.println("Минимальное значение = " + min);

    }

    public static boolean task7Meth(int[] array) {
        int sumL = 0;
        int sumR = 0;

        for (int l = 0; l < array.length - 1; l++) {
            sumL += array[l];
            for (int r = l + 1; r < array.length; r++) {
                sumR += array[r];
            }
            if (sumL == sumR) {
                System.out.println(l);
                return true;
            }
            sumR = 0;
        }
        return false;
    }

    public static void task8(int[] arr, int n) {
        if (n > 0) {
            int value;
            for (int j = 0; j < n; j++) {
                value = arr[arr.length - 1];
                for (int i = arr.length - 1; i > 0; i--) {
                    arr[i] = arr[i - 1];
                }
                arr[0] = value;
            }
        } else if (n < 0) {
            int value;
            for (int j = 0; j < -n; j++) {
                value = arr[0];
                for (int i = 1; i < arr.length ; i++) {
                    arr[i - 1] = arr[i];
                }
                arr[arr.length - 1] = value;
            }
        }

        //check task8
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}

