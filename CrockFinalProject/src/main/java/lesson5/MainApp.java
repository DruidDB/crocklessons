package lesson5;

//1.	Создать массив с набором слов (10–20 слов, должны встречаться повторяющиеся).
// Найти и вывести список уникальных слов, из которых состоит массив (дубликаты не считаем). Посчитать, сколько раз встречается каждое слово.
//2.	Написать простой класс Телефонный Справочник, который хранит в себе список фамилий
// и телефонных номеров. В этот телефонный справочник с помощью метода add() можно добавлять записи,
// а с помощью метода get() искать номер телефона по фамилии. Следует учесть, что под одной фамилией может быть несколько телефонов
// (в случае однофамильцев), и при запросе такой фамилии должны выводиться все телефоны. Желательно не добавлять лишний
// функционал (дополнительные поля — имя, отчество, адрес, — взаимодействие с пользователем через консоль и т. д).
// Консоль использовать только для вывода результатов проверки телефонного справочника.

import java.util.*;

public class MainApp {
    public static void main(String[] args) {
        //System.out.println(task1());

        PhoneDirectory phoneDirectory = new PhoneDirectory();

        phoneDirectory.add("A", "1111");
        phoneDirectory.add("B", "2222");
        phoneDirectory.add("C", "3333");
        phoneDirectory.add("D", "4444");
        phoneDirectory.add("A", "5555");
        phoneDirectory.add("B", "6666");
        phoneDirectory.add("B", "7777");
        phoneDirectory.add("B", "8888");

        System.out.println(phoneDirectory.get("fdaf"));

    }

    public static Map<String, Integer> task1(){
        List<String> list = Arrays.asList("A", "B", "C", "D", "E", "F", "G", "C", "D", "E", "D", "E", "F", "G", "C");
        Map<String, Integer> map = new HashMap<>();

        for (int i = 0; i < list.size(); i++){
            int value = 0;
            for (int j = 0; j < list.size(); j++) {
                if (list.get(i).equals(list.get(j))){
                    value++;
                }
            }
            map.put(list.get(i), value);
        }

        return map;
    }

    public static void task2(){

    }
}