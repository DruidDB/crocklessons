package lesson5;


import java.util.*;

public class PhoneDirectory {
    private Map<String, Set<String>> book;

    public PhoneDirectory() {
        this.book = new HashMap<>();
    }

    public void add(String surname, String phone){
        Set<String> phones = book.getOrDefault(surname , new HashSet<>());
        phones.add(phone);
        book.put(surname, phones);
    }
    public Set<String> get(String surname){
        return book.get(surname);
    }
}
