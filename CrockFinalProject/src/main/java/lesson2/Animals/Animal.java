package lesson2.Animals;

public abstract class Animal {
    private String type;
    private String name;
    private int maxRunDistance;
    private int maxSwimDistance;

    private static int counter = 0;

    public Animal(String type, String name, int maxRunDistance, int maxSwimDistance) {
        this.type = type;
        this.name = name;
        this.maxRunDistance = maxRunDistance;
        this.maxSwimDistance = maxSwimDistance;
        counter++;
    }

    public int getCounter() {
        return counter;
    }

    public void run(int distance) {
        if (maxRunDistance == 0) {
            System.out.println(type + " Не умеет бегать");
            return;
        }
        if (maxRunDistance >= distance) {
            System.out.println(type + " " + name + "- пробежал " + distance + "м. ");
        } else {
            System.out.println(type + " " + name + "- не смог пробежать " + distance + "м. ");
        }
    }


    public void swim(int distance) {
        if (maxSwimDistance == 0) {
            System.out.println(type + " Не умеет плавать");
            return;
        }
        if (maxSwimDistance >= distance) {
            System.out.println(type + " " + name + "- проплыл " + distance + "м. ");
        } else {
            System.out.println(type + " " + name + "- не смог проплыть " + distance + "м. ");
        }
    }
}
