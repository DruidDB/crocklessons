package lesson2.Animals;

public class HomeCat extends Cat {
    private static int counter = 0;

    public HomeCat(String name) {
        super("Кот", name, 200, 0);
        counter++;
    }
}
