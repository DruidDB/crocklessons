package lesson2.Animals;

public abstract class Cat extends Animal {
    private static int counter = 0;

    public Cat(String type, String name, int maxRunDistance, int maxSwimDistance) {
        super(type, name, maxRunDistance, maxSwimDistance);
        counter++;
    }
}
