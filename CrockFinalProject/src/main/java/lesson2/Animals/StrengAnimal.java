package lesson2.Animals;

public class StrengAnimal extends Animal {
    private static int counter = 0;

    public StrengAnimal(String type, String name, int maxRunDistance, int maxSwimDistance) {
        super(type, name, maxRunDistance, maxSwimDistance);
        counter++;
    }
}
