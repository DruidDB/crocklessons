package lesson2.Animals;

public class Dog extends Animal {
    private static int counter = 0;

    public Dog(String name) {
        super("Собака", name, 500, 10);
        counter++;
    }
}
