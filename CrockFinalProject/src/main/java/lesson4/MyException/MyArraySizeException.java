package lesson4.MyException;

public class MyArraySizeException extends Exception {
    public MyArraySizeException() {
        super("The array does not have a size of 4x4");
    }
}
