package lesson4.MyException;

import java.io.IOException;

public class MyArrayDataException extends Exception {
    private int column;
    private int row;

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }

    public MyArrayDataException(int column, int row) {
        super("An erroneous element was found in column: " + column + ", row: " + row);
        this.column = column;
        this.row = row;
    }


}
