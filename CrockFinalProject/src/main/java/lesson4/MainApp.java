package lesson4;

//1.	Создайте исключения: MyArraySizeException (неправильный размер массива), и MyArrayDataException (в ячейке массива лежит что-то не то);
//2.	Напишите метод, на вход которого подаётся двумерный строковый массив (String[][]) размером 4×4. При подаче массива другого размера необходимо бросить исключение MyArraySizeException.
//3.	Далее метод должен пройтись по всем элементам массива, преобразовать в int и просуммировать. Если в каком-то элементе массива преобразование не удалось (например, в ячейке лежит символ или текст вместо числа), должно быть брошено исключение MyArrayDataException с детализацией, в какой именно ячейке лежат неверные данные. Расчет данных для этой матрицы прекращается.
//4.	В методе main() нужно вызвать полученный метод, обработать возможные исключения MySizeArrayException и MyArrayDataException и вывести результат расчёта.

import lesson4.MyException.MyArrayDataException;
import lesson4.MyException.MyArraySizeException;

public class MainApp {
    public static void main(String[] args) {
        String[][] array = {
                {"1", "1", "1", "1"},
                {"1", "1", "1", "1"},
                {"1", "1", "1", "1"},
                {"1", "1", "1", "1"}
        };

        try {
            System.out.println(arrayMethod(array));
        } catch (MyArraySizeException e) {
            e.printStackTrace();
        } catch (MyArrayDataException e) {
            e.printStackTrace();
        }
    }

    public static int arrayMethod(String[][] array) throws MyArraySizeException, MyArrayDataException {
        if (array.length != 4) {
            throw new MyArraySizeException();
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i].length != 4) {
                throw new MyArraySizeException();
            }
        }

        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                try {
                    sum += Integer.valueOf(array[i][j]);
                }catch (NumberFormatException e){
                    throw new MyArrayDataException(i,j);
                }
            }
        }

        return sum;
    }
}
